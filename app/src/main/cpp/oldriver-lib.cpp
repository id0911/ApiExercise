#include <jni.h>
#include <string>
#include "app-utils.h"
#include "app-rsa2sign.h"

#define ENCRYPT_VAL "AD64A5E04899E88ED0F7F213A4DE6A50" //加密串
#define encryption Java_com_quanwe_clib_AppCore_encryption
static bool is_init = false;
/**
 * 检查是否正常初始化
 * @param env
 */
void checkInit(JNIEnv *env) {
    if (!is_init) {
        exitApplication(env, 0);
    }
}

/**
 * 对给定数组进行加密
 * @param env
 * @param type
 * @param content_
 * @return
 */
extern "C"
JNIEXPORT jbyteArray JNICALL
encryption(JNIEnv *env, jclass type, jbyteArray content_) {
    checkInit(env);
    return encrypt(env, content_, ENCRYPT_VAL);
}

extern "C"
JNIEXPORT void JNICALL
Java_com_quanwe_clib_AppCore_init(JNIEnv *env, jclass type, jobject context) {}

extern "C"
JNIEXPORT jstring JNICALL
Java_com_quanwe_clib_AppCore_sign(JNIEnv *env,jclass thiz, jstring content) {
    checkInit(env);
    return RSA2SIGN(env,content);
}

/**
 * jni load
 * @param vm
 * @param reserved
 * @return
 */
JNIEXPORT jint JNICALL JNI_OnLoad(JavaVM* vm, void* reserved){
    LOGE("JNI_OnLoad 初始化");
    JNIEnv* env = NULL;
    if (vm->GetEnv( (void**) &env, JNI_VERSION_1_4) != JNI_OK) {
        return -1;
    }
    is_init = init(env, NULL);
    return JNI_VERSION_1_4;
}

