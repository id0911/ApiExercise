package com.quanwe.common.base

import android.app.Application
import com.quanwe.clib.AppCore
import com.quanwe.common.service.NetTaskService
import org.jetbrains.anko.startService

/**
 * Created by WQ on 2017/5/24.
 */

class App : Application() {
    override fun onCreate() {
        super.onCreate()
        //初始化核心函数库
        AppCore.init(this)
//        startService<NetTaskService>()
    }


   public companion object {
     public open lateinit var _CONTEXT:App
    }
}
