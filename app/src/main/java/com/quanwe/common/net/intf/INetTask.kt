package com.quanwe.common.net.intf

import android.app.Service

/**
 * Created by WQ on 2018/3/27.
 */
interface INetTask {
    fun attatch(context: Service)
    fun execute()
}