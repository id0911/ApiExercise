package com.quanwe.common.net

import com.quanwe.home.bean.Weather
import kotlin.properties.Delegates
import kotlin.properties.ObservableProperty
import kotlin.reflect.KProperty

/**
 * Created by WQ on 2018/4/3.
 */
class ObserveBean {
    val weather: Weather? by Delegates.observable(Weather()) { prop, oldWeather, newWeather ->

    }
}