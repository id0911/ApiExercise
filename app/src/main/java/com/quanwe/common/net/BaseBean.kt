package com.quanwe.common.net

import com.google.gson.annotations.SerializedName

/**
 * Created by WQ on 2017/9/22.
 */

class BaseBean<T> {
    /**
     * status : 1
     * msg : 请求成功
     * page_count : 0
     * page : 1
     * page_size : 10
     */
    @SerializedName(value = "status", alternate = arrayOf("retCode"))
    var status: Int = -1001
    var msg: String = ""
    var page_count: Int = 0
    var page: Int = 0
    var page_size: Int = 0

    @SerializedName(value = "info", alternate = arrayOf("result"))
    var info: T? = null


    fun <U> getFirstData(block: U?.() -> Unit={}): U? {
        val info = this.info
        val result = if (info is List<*> && !info.isEmpty()) {
            info[0] as U?
        } else null
        block(result)
        return result
    }


    override fun toString(): String {
        return info.toString()
    }

    constructor(function: BaseBean<T>.() -> Unit)
    constructor()

}
