package com.quanwe.common.net

import com.quanwe.home.bean.Weather
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * 接口
 * Created by WQ on 2017/9/20.
 */
interface MobAPI {
    companion object {
        val KEY = "24661dd998310"
    }
    //http://apicloud.mob.com/v1/weather/query?key=24661dd998310&city=%E5%A4%A9%E9%97%A8&province=%E6%B9%96%E5%8C%97
    @GET("v1/weather/query")
    fun getWeather(@Query("province") province: String, @Query("city") city: String): Call<BaseBean<List<Weather>>>
    @GET("environment/query")
    fun getEnvironment(@Query("province") province: String, @Query("city") city: String): Call<BaseBean<List<Weather>>>
}