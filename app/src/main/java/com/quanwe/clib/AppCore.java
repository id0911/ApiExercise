package com.quanwe.clib;

import android.content.Context;
import android.util.Base64;
import android.util.Log;

/**
 * Created by WQ on 2018/3/26.
 */

public class AppCore {
   static  {
        System.loadLibrary("oldriver-lib");
    }

    /**
     * 初始化核心库
     */
    public static native void init(Context context);

    /**
     * 加密数据-可逆
     * @param content 字节数组
     */
    public static native byte[] encryption(byte [] content);
    /**
     * 加密数据-非对称加密
     * @param content 待加密字符串
     */
    public static native String sign(String content);

    /**
     * 计算给定内容的签名
     * @param content
     * @return
     */
    public static String getSign(String content){
        Log.e("AppCore", "getSign: "+content.getClass() );
        return sign(content);
    }
    /**
     * 加密字符串
     */
    public static String encryptionStr(String content) {
        return new String(Base64.encode(encryption(content.getBytes()),Base64.DEFAULT));
    }

    /**
     * 解密字符串
     */
    public static String decryptionStr(String content )  {
        return new String(encryption(Base64.decode(content.getBytes(), Base64.DEFAULT)));
    }
}
