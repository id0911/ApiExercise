package com.quanwe.home.bean

/**
 * Created by WQ on 2018/4/3.
 */
class Weather {
    /**
     * airCondition : 良
     * city : 天门
     * coldIndex : 低发期
     * date : 2018-04-03
     * distrct : 天门
     * dressingIndex : 薄短袖类
     * exerciseIndex : 不太适宜
     * future : [{"date":"2018-04-03","dayTime":"阴","night":"阴","temperature":"30°C / 17°C","week":"今天","wind":"南风 小于3级"},{"date":"2018-04-04","dayTime":"阴","night":"中雨","temperature":"17°C / 10°C","week":"星期三","wind":"东北风 3～4级"},{"date":"2018-04-05","dayTime":"小雨","night":"小雨","temperature":"10°C / 8°C","week":"星期四","wind":"东北风 3～4级"},{"date":"2018-04-06","dayTime":"晴","night":"多云","temperature":"18°C / 8°C","week":"星期五","wind":"东北风 小于3级"},{"date":"2018-04-07","dayTime":"多云","night":"多云","temperature":"19°C / 8°C","week":"星期六","wind":"西南风 小于3级"},{"date":"2018-04-08","dayTime":"晴","night":"多云","temperature":"22°C / 10°C","week":"星期日","wind":"西南风 小于3级"},{"date":"2018-04-09","dayTime":"晴","night":"少云","temperature":"26°C / 14°C","week":"星期一","wind":"西南偏南风 3级"},{"date":"2018-04-10","dayTime":"局部多云","night":"阵雨","temperature":"26°C / 13°C","week":"星期二","wind":"东北偏东风 3级"},{"date":"2018-04-11","dayTime":"阵雨","night":"局部多云","temperature":"21°C / 12°C","week":"星期三","wind":"东北偏北风 3级"}]
     * humidity : 湿度：55%
     * pollutionIndex : 78
     * province : 湖北
     * sunrise : 06:17
     * sunset : 18:50
     * temperature : 29℃
     * time : 13:52
     * updateTime : 20180403141000
     * washIndex : 不适宜
     * weather : 晴
     * week : 周二
     * wind : 东南风2级
     */

    var airCondition: String? = null
    var city: String? = null
    var coldIndex: String? = null
    var date: String? = null
    var distrct: String? = null
    var dressingIndex: String? = null
    var exerciseIndex: String? = null
    var humidity: String? = null
    var pollutionIndex: String? = null
    var province: String? = null
    var sunrise: String? = null
    var sunset: String? = null
    var temperature: String? = null
    var time: String? = null
    var updateTime: String? = null
    var washIndex: String? = null
    var weather: String? = null
    var week: String? = null
    var wind: String? = null
    /**
     * date : 2018-04-03
     * dayTime : 阴
     * night : 阴
     * temperature : 30°C / 17°C
     * week : 今天
     * wind : 南风 小于3级
     */

    var future: List<FutureBean>? = null

    class FutureBean {
        var date: String? = null
        var dayTime: String? = null
        var night: String? = null
        var temperature: String? = null
        var week: String? = null
        var wind: String? = null
    }
}