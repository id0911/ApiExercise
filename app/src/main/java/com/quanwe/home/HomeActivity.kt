package com.quanwe.home

import android.os.Bundle
import com.quanwe.R
import com.quanwe.common.base.BaseActivity
import com.quanwe.common.net.BaseBean
import com.quanwe.common.util.isAsynOK
import com.quanwe.common.util.mobApi
import com.quanwe.home.bean.Weather
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.include_head_weather.*
import org.jetbrains.anko.backgroundResource

class HomeActivity : BaseActivity() {
    override fun onViewCreated(savedInstanceState: Bundle?) {
        layHeadAd.backgroundResource=R.color.title_background_color
        mobApi.getWeather("湖北", "武汉").isAsynOK {
            getFirstData<Weather>()?.apply { weatherLoad(this) }
        }
        mobApi.getWeather("湖北", "武汉").isAsynOK (::print)
    }
//    callback: BaseBean<U>.() -> Unit

  public  inline fun  weather(bean:Any){

}

    fun weatherLoad(weather: Weather) {
        temperatureText.text=weather.temperature?.replace("℃","")
        addressText.text=weather.city
        qualityText.text="空气质量:${weather.pollutionIndex}"
    }


    override fun getLayoutId(): Int = R.layout.activity_home

}
