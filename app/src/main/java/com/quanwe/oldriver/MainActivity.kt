package com.quanwe.oldriver

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import com.quanwe.R
import com.quanwe.clib.AppCore.*
import com.quanwe.common.util._Log
import com.quanwe.home.HomeActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*

class MainActivity : AppCompatActivity() {
    var isEncryption = false
    var lastPosition=0;
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        appBarLay.addOnOffsetChangedListener{
            appBarLayout, verticalOffset ->


        }
        fab.setOnClickListener { view ->
            sample_text.text = getTxtContent(sample_text.text.toString())
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()
        }

        // Example of a call to a native method
        sample_text.text = getTxtContent("O(∩_∩)O哈哈MainActivity O(∩_∩)O哈哈~")
        /**
         * rsa2签名测试
         */
        val strVal="app_id=2014072300007148&biz_content={\"button\":[{\"actionParam\":\"ZFB_HFCZ\",\"actionType\":\"out\",\"name\":\"话费充值\"},{\"name\":\"查询\",\"subButton\":[{\"actionParam\":\"ZFB_YECX\",\"actionType\":\"out\",\"name\":\"余额查询\"},{\"actionParam\":\"ZFB_LLCX\",\"actionType\":\"out\",\"name\":\"流量查询\"},{\"actionParam\":\"ZFB_HFCX\",\"actionType\":\"out\",\"name\":\"话费查询\"}]},{\"actionParam\":\"http://m.alipay.com\",\"actionType\":\"link\",\"name\":\"最新优惠\"}]}&charset=GBK&method=alipay.mobile.public.menu.add&sign_type=RSA2&timestamp=2014-07-24 03:07:50&version=1.0";
        strVal._Log()
        val signResult=getSign(strVal)
        signResult._Log()
        sample_text2.text=signResult
    }

    fun getTxtContent(content: String): String {
        val result = if (isEncryption) decryptionStr(content) else encryptionStr(content)
        isEncryption = !isEncryption
        return result
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        Snackbar.make(toolbar, item.title, Snackbar.LENGTH_SHORT)
                .setAction("Action", {
                    Toast.makeText(this, "Action", Toast.LENGTH_SHORT).show()
                }).show()
        return when (item.itemId) {
            R.id.action_home->{
                startActivity(Intent(this, HomeActivity::class.java))
                true
            }
            R.id.action_settings -> {
                startActivity(Intent(this, SettingsActivity::class.java))
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    /**
     * A native method that is implemented by the 'native-lib' native library,
     * which is packaged with this application.
     */

    companion object {

        init {
            System.loadLibrary("oldriver-lib")
        }
    }
}
