package com.quanwe;

import java.util.List;

/**
 * Created by WQ on 2018/4/3.
 */

public class TestClass {

    /**
     * airCondition : 良
     * city : 天门
     * coldIndex : 低发期
     * date : 2018-04-03
     * distrct : 天门
     * dressingIndex : 薄短袖类
     * exerciseIndex : 不太适宜
     * future : [{"date":"2018-04-03","dayTime":"阴","night":"阴","temperature":"30°C / 17°C","week":"今天","wind":"南风 小于3级"},{"date":"2018-04-04","dayTime":"阴","night":"中雨","temperature":"17°C / 10°C","week":"星期三","wind":"东北风 3～4级"},{"date":"2018-04-05","dayTime":"小雨","night":"小雨","temperature":"10°C / 8°C","week":"星期四","wind":"东北风 3～4级"},{"date":"2018-04-06","dayTime":"晴","night":"多云","temperature":"18°C / 8°C","week":"星期五","wind":"东北风 小于3级"},{"date":"2018-04-07","dayTime":"多云","night":"多云","temperature":"19°C / 8°C","week":"星期六","wind":"西南风 小于3级"},{"date":"2018-04-08","dayTime":"晴","night":"多云","temperature":"22°C / 10°C","week":"星期日","wind":"西南风 小于3级"},{"date":"2018-04-09","dayTime":"晴","night":"少云","temperature":"26°C / 14°C","week":"星期一","wind":"西南偏南风 3级"},{"date":"2018-04-10","dayTime":"局部多云","night":"阵雨","temperature":"26°C / 13°C","week":"星期二","wind":"东北偏东风 3级"},{"date":"2018-04-11","dayTime":"阵雨","night":"局部多云","temperature":"21°C / 12°C","week":"星期三","wind":"东北偏北风 3级"}]
     * humidity : 湿度：55%
     * pollutionIndex : 78
     * province : 湖北
     * sunrise : 06:17
     * sunset : 18:50
     * temperature : 29℃
     * time : 13:52
     * updateTime : 20180403141000
     * washIndex : 不适宜
     * weather : 晴
     * week : 周二
     * wind : 东南风2级
     */

    private String airCondition;
    private String city;
    private String coldIndex;
    private String date;
    private String distrct;
    private String dressingIndex;
    private String exerciseIndex;
    private String humidity;
    private String pollutionIndex;
    private String province;
    private String sunrise;
    private String sunset;
    private String temperature;
    private String time;
    private String updateTime;
    private String washIndex;
    private String weather;
    private String week;
    private String wind;
    /**
     * date : 2018-04-03
     * dayTime : 阴
     * night : 阴
     * temperature : 30°C / 17°C
     * week : 今天
     * wind : 南风 小于3级
     */

    private List<FutureBean> future;

    public String getAirCondition() {
        return airCondition;
    }

    public void setAirCondition(String airCondition) {
        this.airCondition = airCondition;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getColdIndex() {
        return coldIndex;
    }

    public void setColdIndex(String coldIndex) {
        this.coldIndex = coldIndex;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDistrct() {
        return distrct;
    }

    public void setDistrct(String distrct) {
        this.distrct = distrct;
    }

    public String getDressingIndex() {
        return dressingIndex;
    }

    public void setDressingIndex(String dressingIndex) {
        this.dressingIndex = dressingIndex;
    }

    public String getExerciseIndex() {
        return exerciseIndex;
    }

    public void setExerciseIndex(String exerciseIndex) {
        this.exerciseIndex = exerciseIndex;
    }

    public String getHumidity() {
        return humidity;
    }

    public void setHumidity(String humidity) {
        this.humidity = humidity;
    }

    public String getPollutionIndex() {
        return pollutionIndex;
    }

    public void setPollutionIndex(String pollutionIndex) {
        this.pollutionIndex = pollutionIndex;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getSunrise() {
        return sunrise;
    }

    public void setSunrise(String sunrise) {
        this.sunrise = sunrise;
    }

    public String getSunset() {
        return sunset;
    }

    public void setSunset(String sunset) {
        this.sunset = sunset;
    }

    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public String getWashIndex() {
        return washIndex;
    }

    public void setWashIndex(String washIndex) {
        this.washIndex = washIndex;
    }

    public String getWeather() {
        return weather;
    }

    public void setWeather(String weather) {
        this.weather = weather;
    }

    public String getWeek() {
        return week;
    }

    public void setWeek(String week) {
        this.week = week;
    }

    public String getWind() {
        return wind;
    }

    public void setWind(String wind) {
        this.wind = wind;
    }

    public List<FutureBean> getFuture() {
        return future;
    }

    public void setFuture(List<FutureBean> future) {
        this.future = future;
    }

    public static class FutureBean {
        private String date;
        private String dayTime;
        private String night;
        private String temperature;
        private String week;
        private String wind;

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getDayTime() {
            return dayTime;
        }

        public void setDayTime(String dayTime) {
            this.dayTime = dayTime;
        }

        public String getNight() {
            return night;
        }

        public void setNight(String night) {
            this.night = night;
        }

        public String getTemperature() {
            return temperature;
        }

        public void setTemperature(String temperature) {
            this.temperature = temperature;
        }

        public String getWeek() {
            return week;
        }

        public void setWeek(String week) {
            this.week = week;
        }

        public String getWind() {
            return wind;
        }

        public void setWind(String wind) {
            this.wind = wind;
        }
    }
}
